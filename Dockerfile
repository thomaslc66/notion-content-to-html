FROM node:20.9

WORKDIR /app

COPY package.json /app 

RUN npm install --no-optional && npm cache clean --force

EXPOSE 3000

COPY . /app
COPY docker.env /app/.env

CMD ["node","./bin/www"]